namespace DatingAppAPI.Models
{
    public class Photo
    {
        public int Id { get; set; }
        public bool IsMainPhoto { get; set; }
        public string URL { get; set; }
        public string Description{get;set;}
        public User User{get;set;}
        public int UserId{get;set;} 
        public string PublicId {get;set;}

    }
}