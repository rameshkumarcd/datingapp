using System;
using System.Security.Claims;
using System.Threading.Tasks;
using DatingAppAPI.Data;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
namespace DatingAppAPI.Helpers
{
    public class UserActivityFilter : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var resultContext= await next();
            var userId= int.Parse(context.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
            var userRepository = context.HttpContext.RequestServices.GetService<IDatingRepository>();
            var user= await userRepository.GetUser(userId);
            user.LastActive = DateTime.Now;
            await userRepository.SaveAll();
        }
    }
}