using DatingAppAPI.DTOs;
using DatingAppAPI.Models;
using AutoMapper;
using System.Linq;
using System;

namespace DatingAppAPI.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<User, DetailedUserDTO>()
            .ForMember(d=>d.MainPhotoUrl,  s=>{
                s.MapFrom(src=>src.Photos.Where(p=>p.IsMainPhoto==true).FirstOrDefault().URL);
            })
            .ForMember(d=>d.Age,  s=>{
                s.MapFrom(src=> CalculateAge(src.DOB));
            });
            CreateMap<User, UserForListDTO>()
            .ForMember(d=>d.MainPhotoUrl,  s=>{
                s.MapFrom(src=>src.Photos.Where(p=>p.IsMainPhoto==true).FirstOrDefault().URL);
            })
            .ForMember(d=>d.Age,  s=>{
                s.MapFrom(src=> CalculateAge(src.DOB));
            });
            CreateMap<Photo, PhotoDTO>();
            CreateMap<MemberProfileDTO, User>();
            CreateMap<NewPhotoDTO, Photo>();
            CreateMap<User, UserForRegistrationDTO>().ReverseMap().ForMember(d=>d.DOB, s=>{
                s.MapFrom(src=> src.DateOfBirth);
            });
        }

        private   int CalculateAge(DateTime dob){
            int age= DateTime.Now.Year- dob.Year;
            if ( dob.AddYears(age) > DateTime.Now){
                age=age-1;
            }
            return age;
        }

    }
}