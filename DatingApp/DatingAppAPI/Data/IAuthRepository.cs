
using DatingAppAPI.Models;
using System.Threading.Tasks;

namespace DatingAppAPI.Data
{
    public interface IAuthRepository
    {
         Task<User> Register(User user, string password);

         Task<User> Login (User user,string password);

         Task<bool> IsUserExist(string username);
    }
}