using System.Collections.Generic;
using System.IO;
using System.Linq;
using DatingAppAPI.Models;
using Newtonsoft.Json;

namespace DatingAppAPI.Data{

public class Seed{
    public static void SeedUsersData(DataContext context)
    {
        if (context.Users.Any()){
            return;
        }
        string userData = File.ReadAllText("Data/UserSeedData.json");
        var users = JsonConvert.DeserializeObject<List<User>>(userData);
        foreach(var user in users){
            AuthRepository.CreatePasswordHash(user, "password");
            context.Users.Add(user);
        }
        context.SaveChanges();
    }
}

}