using System;
using System.Threading.Tasks;
using DatingAppAPI.Models;
using System.Security.Cryptography;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace DatingAppAPI.Data
{
    public class AuthRepository : IAuthRepository
    {
        private DataContext _context;
        public AuthRepository(DataContext context)
        {
            _context = context;
        }
        public async Task<bool> IsUserExist(string username)
        {
            var registeredUser= await _context.Users.FirstOrDefaultAsync(u => u.Username == username);
            return registeredUser != null;
        }

        public async Task<User> Login(User user, string password)
        {
            var registeredUser= await _context.Users.Include(p=>p.Photos).FirstOrDefaultAsync(u => u.Username == user.Username);
            if (registeredUser == null){
                return registeredUser;
            }
            if (VerifyPassword(registeredUser, password)){
                return registeredUser;
            }
            else{
                return null;
            }
        }

        public async Task<User>  Register(User user, string password)
        {
            CreatePasswordHash(user, password);
            await _context.Users.AddAsync(user);
            await _context.SaveChangesAsync();
            return user;
        }

        public static void CreatePasswordHash(User user, string password)
        {
            using (var hmac= new  HMACSHA512()){
                user.SaltPassword = hmac.Key;
                user.HashPassword  = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
            }           
        }

        private bool VerifyPassword(User user, string password){
            using (var hmac= new  HMACSHA512(user.SaltPassword)){
                
                var computedHash  = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
                for(int index=0; index<= computedHash.Length-1; index++){
                    if (computedHash[index] != user.HashPassword[index]){
                        return false;
                    }
                }

                return true;
            }
        }
    }
}