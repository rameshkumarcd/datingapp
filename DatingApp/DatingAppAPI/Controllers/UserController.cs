using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using DatingAppAPI.Data;
using DatingAppAPI.DTOs;
using DatingAppAPI.Helpers;
using DatingAppAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DatingAppAPI.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserController : ControllerBase
    {
        private IDatingRepository _datingRepository=null;
        private IMapper _mapper=null;
        public UserController(IDatingRepository datingRepository, IMapper mapper)
        {
            _datingRepository = datingRepository;

            _mapper = mapper;
        }

        [HttpGet("{userId}")]
        [ServiceFilter(typeof(UserActivityFilter))]
        public async Task<IActionResult> GetUser(int userId){
            
            User user= await _datingRepository.GetUser(userId);
            var returnUser = _mapper.Map<DetailedUserDTO>(user);
            return Ok(returnUser);

        }
        [HttpGet]
        public async Task<IActionResult> GetUsers(){
            
            var users= await _datingRepository.GetUsers();
            var returnUsers = _mapper.Map<IEnumerable<UserForListDTO>>(users);
            return Ok(returnUsers);

        }

        [HttpPut("{userId}")]
        public async Task<IActionResult> UpdateUser(int userId, MemberProfileDTO memberProfileDTO){
            if (userId != int.Parse( User.FindFirst(ClaimTypes.NameIdentifier).Value)){
                return Unauthorized();
            }

            User user = await _datingRepository.GetUser(userId);
            _mapper.Map(memberProfileDTO, user);

            if (await _datingRepository.SaveAll()){
                return NoContent();
            }else{
                throw new System.Exception($"Error when updating the user {user.Username}.");
            }

        }
    }
}