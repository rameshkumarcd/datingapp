using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using DatingAppAPI.Helpers;
using CloudinaryDotNet;
using DatingAppAPI.DTOs;
using System.Security.Claims;
using System.Threading.Tasks;
using CloudinaryDotNet.Actions;
using DatingAppAPI.Data;
using AutoMapper;
using DatingAppAPI.Models;
using System.Linq;

namespace DatingAppAPI.Controllers
{
    [Route("api/user/{userId}/photo")]
    [ApiController]
    [Authorize]
    public class PhotoController : ControllerBase
    {
        private Cloudinary _cloudinary=null;
        
        private IDatingRepository _datingRepository=null;
        private IMapper _mapper=null;
        public PhotoController(IOptions<CloudinarySettings> cloudinaryConfig
        , IDatingRepository datingRepository, IMapper mapper )
        {
            
            Account cloudinaryAccount=new Account(){
                ApiKey = cloudinaryConfig.Value.Key,
                ApiSecret = cloudinaryConfig.Value.Secret,
                Cloud = cloudinaryConfig.Value.Name
            };

            this._cloudinary = new Cloudinary(cloudinaryAccount);

            _datingRepository = datingRepository;

            _mapper = mapper;

        }

        [HttpPost]
        public async Task<IActionResult> AddPhoto(int userId, [FromForm] NewPhotoDTO newPhotoDTO){
            if (userId != int.Parse( User.FindFirst(ClaimTypes.NameIdentifier).Value)){
                return Unauthorized();
            }

            ImageUploadResult uploadResult=null;
            using (var fileStream = newPhotoDTO.File.OpenReadStream()){
                var uploadParameters = new ImageUploadParams(){
                    File = new FileDescription(newPhotoDTO.File.Name, fileStream),
                    Transformation = new Transformation().Width(500).Height(500).Crop("fill").Gravity("face")
                };
                uploadResult = _cloudinary.Upload(uploadParameters); 
            }

            User user = await _datingRepository.GetUser(userId);
            Photo photo= _mapper.Map<Photo>(newPhotoDTO);
            photo.URL = uploadResult.Url.ToString();
            photo.PublicId = uploadResult.PublicId;
            photo.IsMainPhoto = !user.Photos.Any(p=>p.IsMainPhoto == true);
            user.Photos.Add(photo);
            
            if (await _datingRepository.SaveAll()){
                var photoDTO=_mapper.Map<PhotoDTO>(photo);
                return CreatedAtRoute("GetPhoto", new {userId= user.Id,id = photo.Id}, photoDTO);
            }else{
                throw new System.Exception($"Error when adding new photo for the user {user.Username}.");
            }

        }

        [HttpGet("{id}", Name="GetPhoto")]
        public async Task<IActionResult> GetPhoto(int id){
            var photo= await  _datingRepository.GetPhoto(id);
            var photoDTO= _mapper.Map<PhotoDTO>(photo);
            return Ok(photoDTO);
        }

        [HttpPost("{id}/setMain")]
        public async Task<IActionResult> SetMainPhoto(int userId, int id){
            if (userId != int.Parse( User.FindFirst(ClaimTypes.NameIdentifier).Value)){
                return Unauthorized();
            }
            var user = await _datingRepository.GetUser(userId);
            if (!user.Photos.Any(p=>p.Id == id)){
                return Unauthorized();
            }

            var mainPhoto= user.Photos.Where(p=>p.IsMainPhoto).FirstOrDefault();
            if (mainPhoto != null){
                mainPhoto.IsMainPhoto =false;
            }

            var currentPhoto= user.Photos.Where(p=>p.Id==id).FirstOrDefault();
            currentPhoto.IsMainPhoto=true;

            
            if (await _datingRepository.SaveAll()){
                return NoContent();
            }
            return BadRequest("Unable to set main photo");

        }

        [HttpDelete("{id}/deletephoto")]
        public async Task<IActionResult> DeletePhoto(int userId, int id){
            if (userId != int.Parse( User.FindFirst(ClaimTypes.NameIdentifier).Value)){
                return Unauthorized();
            }

            var user = await _datingRepository.GetUser(userId);
            if (!user.Photos.Any(p=>p.Id == id)){
                return Unauthorized();
            }
            var photo= user.Photos.Where(p=>p.Id==id).FirstOrDefault();
            if (photo.IsMainPhoto){
                return BadRequest("cannot delete main photo.");
            }

            if (photo.PublicId != null){
                var deletionResult= await _cloudinary.DestroyAsync(new DeletionParams(photo.PublicId));
                if (deletionResult.Result != "ok"){
                    throw new System.Exception("Unable to delete from image storage.");
                }
            }
            user.Photos.Remove(photo);

            if (await _datingRepository.SaveAll()){
                return Ok();
            }
        
            return BadRequest("Unable to delete photo");
            
        }
    }
}