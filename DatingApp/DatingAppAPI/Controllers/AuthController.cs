using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using DatingAppAPI.Data;
using DatingAppAPI.DTOs;
using DatingAppAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace DatingAppAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private IAuthRepository _authRepository=null;
        private IConfiguration _config = null;

        private IMapper _mapper = null;

        public AuthController(IAuthRepository authRepository
        , IConfiguration config
        , IMapper mapper)
        {
            _authRepository = authRepository;
            _config = config;
            _mapper = mapper;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register(UserForRegistrationDTO userForRegistrationDTO){
            userForRegistrationDTO.DateOfBirth = userForRegistrationDTO.DateOfBirth.ToLocalTime();
            userForRegistrationDTO.Username = userForRegistrationDTO.Username.ToLower();
            if (await _authRepository.IsUserExist(userForRegistrationDTO.Username)){
                return BadRequest("Username already exists");
            }

            //User userTobeCreated=new User () {Username= userForRegistrationDTO.Username };
            User userTobeCreated= _mapper.Map<User>(userForRegistrationDTO);
            userTobeCreated = await _authRepository.Register(userTobeCreated, userForRegistrationDTO.Password);
            return StatusCode(201);

        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(UserForLoginDTO userForLoginDTO){     
            User authenticatedUser = await Authenticate(userForLoginDTO);
            if (authenticatedUser == null){
                return Unauthorized();
            }
            string jwtToken = GenerateToken(authenticatedUser);
            var photo= authenticatedUser.Photos.Where(p=>p.IsMainPhoto).FirstOrDefault();
            string photoUrl= string.Empty;
            if (photo!= null){
                photoUrl= photo.URL;
            }
            return Ok(new {token = jwtToken, photoUrl= photoUrl });
        }

        private async Task<User> Authenticate(UserForLoginDTO userForLoginDTO){
            User loginRequestedUser=new User {Username= userForLoginDTO.Username.ToLower() };
            User authenticatedUser= await _authRepository.Login(loginRequestedUser, userForLoginDTO.Password);
            return authenticatedUser;
        }

        private string GenerateToken(User authenticatedUser){
            var claims=new []{new Claim(ClaimTypes.NameIdentifier, authenticatedUser.Id.ToString() ),
                                new Claim(ClaimTypes.Name, authenticatedUser.Username  )
                            };
            var key= new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.GetSection("AppSettings:Token").Value));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);
            var tokenDescriptor= new SecurityTokenDescriptor{ Subject = new ClaimsIdentity(claims)
                                 , Expires=DateTime.Now.AddHours(2), SigningCredentials = credentials };
            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);

            
        }
    }
}