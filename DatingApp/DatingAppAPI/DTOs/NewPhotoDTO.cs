using System;
using Microsoft.AspNetCore.Http;

namespace DatingAppAPI.DTOs
{
    public class NewPhotoDTO
    {
        public string URL { get; set; }
        public IFormFile File { get; set; }
        public DateTime DateAdded { get; set; }
        public string PublicId { get; set; }
        public string Description{get;set;}

        public NewPhotoDTO()
        {
            DateAdded = DateTime.Now;
        }

    }
}