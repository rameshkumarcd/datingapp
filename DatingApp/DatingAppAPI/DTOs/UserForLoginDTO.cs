using System.ComponentModel.DataAnnotations;
namespace DatingAppAPI.DTOs
{
    public class UserForLoginDTO
    {
        [Required]
        public string Username{get;set;}

        [Required]
        [StringLength(10,MinimumLength=5, ErrorMessage="Password should be between 5 and 10 characters")]
        public string Password{get;set;}
    }
}