namespace DatingAppAPI.DTOs
{
    public class PhotoDTO
    {
        public int Id { get; set; }
        public bool IsMainPhoto { get; set; }
        public string URL { get; set; }
        public string Description{get;set;}
        public string PublicId {get;set;}
    }
}