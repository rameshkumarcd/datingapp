using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DatingAppAPI.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace DatingAppAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host= CreateHostBuilder(args).Build();
            //call Seed.SeedUsersData

            //Get Context Service and call Seed
                //Get Services
            using(var scope= host.Services.CreateScope()){
                var services= scope.ServiceProvider;
                
                try{
                    //Get Context
                    var context= services.GetRequiredService<DataContext>();
                    Seed.SeedUsersData(context);
                }
                catch(Exception ex){
                    var logger= services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "Error when seed the user data.");
                }
            }

            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
