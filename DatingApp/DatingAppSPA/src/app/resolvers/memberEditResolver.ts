import {Injectable} from "@angular/core";
import {Resolve, Router, ActivatedRouteSnapshot} from "@angular/router";
import {User} from "../models/user"
import { Observable, of } from "rxjs";
import { UserService } from '../services/user.service';
import { AuthService } from '../services/auth.service';
import { AlertifyService } from '../services/alertify.service';
import { catchError } from "rxjs/operators";
import { JwtHelperService } from "@auth0/angular-jwt";

@Injectable()
export class MemberEditResolver implements Resolve<User> {

    constructor(private userService: UserService
        ,private router: Router
        ,private alertifyService: AlertifyService
        ) {}

    resolve(route: ActivatedRouteSnapshot): Observable<User>{
        return this.userService.getUser(new JwtHelperService().decodeToken(localStorage.getItem("token")).nameid).pipe(
            catchError(error=>{
                this.alertifyService.error("Error when retreving user to edit profile.");
                this.router.navigate(["/members"]);
                return of(null);
            })
        );
    }
    
}
