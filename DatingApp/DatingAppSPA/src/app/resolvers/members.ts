import {Injectable} from "@angular/core";
import {Resolve, Router, ActivatedRouteSnapshot} from "@angular/router";
import {User} from "../models/user"
import { Observable, of } from "rxjs";
import { UserService } from '../services/user.service';
import { AlertifyService } from '../services/alertify.service';
import { catchError } from "rxjs/operators";

@Injectable()
export class MembersResolver implements Resolve<User[]> {

    constructor(private userService: UserService
        ,private router: Router
        ,private alertifyService: AlertifyService) {}

    resolve(route: ActivatedRouteSnapshot): Observable<User[]>{
        return this.userService.getUsers().pipe(
            catchError(error=>{
                this.alertifyService.error("Error when retreving members");
                this.router.navigate(["/home"]);
                return of(null);
            })
        );
    }
    
}
