import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { AuthService } from '../services/auth.service';
import {AlertifyService} from '../services/alertify.service';
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private alertifyService: AlertifyService, private router: Router){}
  canActivate(): boolean {
    if (this.authService.IsUserLoggedIn()){
      return true;
    }

    this.alertifyService.error("Not authorized");
    this.router.navigate(['/home']);
    return false;
  }
  
}
