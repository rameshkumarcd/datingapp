import { Injectable } from '@angular/core';
import {  CanDeactivate } from '@angular/router';
import { MemberEditComponent } from '../members/memberEdit/memberEdit.component'

@Injectable({
  providedIn: 'root'
})
export class PreventUnsavedChangesGuard implements CanDeactivate<MemberEditComponent> {
  
  canDeactivate(component: MemberEditComponent): boolean {
    if (component.profileForm.dirty){
      return confirm('There are unsaved changes. Do you want to continue without saving?');
    }
    return true;

  }
  
}
