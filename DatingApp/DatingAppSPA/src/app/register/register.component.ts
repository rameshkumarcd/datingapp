import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from '../services/auth.service';
import {AlertifyService} from '../services/alertify.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
model: any={};
registerForm: FormGroup;

@Input() valuesFromParent:string;
@Output() cancelRegister=new EventEmitter();
bsConfig: Partial<BsDatepickerConfig>;
  constructor(private authService: AuthService
    , private alertifyService: AlertifyService
    , private formBuilder: FormBuilder
    , private router: Router
    ) { }

  ngOnInit() {
    this.model.username="";
    this.model.password="";
    this.bsConfig = {
      containerClass : "theme-red"
    }

    /*this.registerForm = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', 
        [Validators.required, Validators.minLength(4), Validators.maxLength(8)]),
      confirmPassword: new FormControl('', Validators.required)
    }, this.passwordMatchValidator);*/

    this.createRegisterForm();
  }

  createRegisterForm(){
    this.registerForm = this.formBuilder.group({
      gender: ['male'],
      username: ['', Validators.required],
      knownAs: ['', Validators.required],
      dateOfBirth:[null, Validators.required],
      city: ['', Validators.required],
      country: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(8)]],
      confirmPassword: ['', Validators.required]
    }, {validator: this.passwordMatchValidator});
  }

  passwordMatchValidator(formgroup: FormGroup){
    if (formgroup.get('password').value === formgroup.get('confirmPassword').value){
      return null;
    }
    else{
      return {'mismatch':true};
    }
  }

  register(){
    let user = Object.assign({}, this.registerForm.value);
    this.authService.register(user).subscribe(() => {
      
      this.alertifyService.success('registered successfully.');
    }, error=>{
      
      this.alertifyService.error("registration failed :" + error);
    }, ()=>{
      this.authService.login(user).subscribe(()=>{
        this.router.navigate(['/members']);
      });
    });

    console.log(this.registerForm.value);
   
  }

  cancel(){
    this.cancelRegister.emit(false);
    this.alertifyService.message("Cancelled the registration" );
  }

}
