import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { User } from 'src/app/models/user';
import { ActivatedRoute } from '@angular/router';
import { AlertifyService } from 'src/app/services/alertify.service';
import { NgForm } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { TimeagoModule } from 'ngx-timeago';

@Component({
  selector: 'app-memberEdit',
  templateUrl: './memberEdit.component.html',
  styleUrls: ['./memberEdit.component.css']
})
export class MemberEditComponent implements OnInit {
  user: User;
  @HostListener('window:beforeunload', ['$event'])

  @ViewChild("profileForm") 
  profileForm: NgForm;
  
  unloadNotification($event: any){
    if (this.profileForm.dirty){
      $event.returnValue= true;
    }
  }

  constructor(private route: ActivatedRoute, private alertifyService: AlertifyService
    , private userService: UserService) { }

  ngOnInit() {
    this.route.data.subscribe(userResult=>{
      this.user = userResult['user'];
    });
  }

  updateUser(){
    this.userService.updateUser(this.user.id, this.user).subscribe(next=>{
      this.alertifyService.message('User updated.');
      this.profileForm.reset(this.user);
    }, error=>{
      this.alertifyService.error(error);
    });
    
  }

  updateMainPhotoUrl(photoUrl){
    this.user.mainPhotoUrl = photoUrl;
  }

}
