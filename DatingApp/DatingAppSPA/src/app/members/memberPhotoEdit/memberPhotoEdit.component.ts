import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Photo } from 'src/app/models/Photo';
import { FileUploader } from 'ng2-file-upload';
import { environment } from 'src/environments/environment';
import {JwtHelperService} from "@auth0/angular-jwt";
import { AlertifyService } from 'src/app/services/alertify.service';
import { UserService } from 'src/app/services/user.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-memberPhotoEdit',
  templateUrl: './memberPhotoEdit.component.html',
  styleUrls: ['./memberPhotoEdit.component.css']
})
export class MemberPhotoEditComponent implements OnInit {

  uploader:FileUploader;
  hasBaseDropZoneOver: boolean;
  baseUrl= environment.apiUrl;
  response:string;

  constructor(private alertifyService: AlertifyService
    , private userService: UserService
    , private authService: AuthService) { }
  @Input() photos : Photo[];
  @Output() mainPhotoUrlChange = new EventEmitter<string>();
  ngOnInit() {
    this.initializeFileUploader();
  }

  fileOverBase(e:any):void {
    this.hasBaseDropZoneOver = e;
  }

  initializeFileUploader(){
    let token= localStorage.getItem("token");
    //console.log(token);
    this.uploader = new FileUploader({

      url:this.baseUrl + "user/" + new JwtHelperService(token).decodeToken(token).nameid + "/photo",
      authToken: "Bearer " +token,
      isHTML5: true,
      allowedFileType: ["image"],
      removeAfterUpload: true,
      autoUpload:false,
      maxFileSize: 10 * 1024 * 1024 
    });

    this.uploader.onAfterAddingFile = (file)=>{file.withCredentials = false;}

    this.uploader.onSuccessItem = (item, response, status, headers) =>{
      if (response){
        
        let res:Photo = JSON.parse(response);
        this.photos.push(res);
        if (res.isMainPhoto){
          this.mainPhotoUrlChange.emit(res.url);
          this.authService.changeUserPhotoUrl(res.url);
          localStorage.setItem("photo", res.url);
        }

        }
      }
    }
    
    setMainPhoto(photo: Photo){
      let token= localStorage.getItem("token");
      this.userService.setMainPhoto(new JwtHelperService(token).decodeToken(token).nameid, photo.id).subscribe(next=>{
        for (const p of this.photos) {
          if (p.id == photo.id){
            p.isMainPhoto = true;
          }
          else{
            p.isMainPhoto = false;
          }
        }
        this.mainPhotoUrlChange.emit(photo.url);
        this.authService.changeUserPhotoUrl(photo.url);
        localStorage.setItem("photo", photo.url);
        this.alertifyService.message('Main Photo updated');
      }, error=>{
        this.alertifyService.error(error);
      });
    }

    deletePhoto(photo: Photo){
      let token= localStorage.getItem("token");
      this.alertifyService.confirm('do you want to delete this photo?', ()=> {
      this.userService.deletePhoto(new JwtHelperService(token).decodeToken(token).nameid, photo.id).subscribe(next=>{
        this.photos.splice(this.photos.findIndex(p=>p.id === photo.id),1);
        this.alertifyService.success('Photo has been deleted.')
      }, error=>{
        this.alertifyService.error(error);
      });
    });
    }

  }

