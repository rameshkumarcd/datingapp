import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { AlertifyService } from '../../services/alertify.service';
import { User } from '../../models/user';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-membersList',
  templateUrl: './membersList.component.html',
  styleUrls: ['./membersList.component.css']
})
export class MembersListComponent implements OnInit {

  constructor(private userService: UserService, private alertifyService: AlertifyService,
    private route:ActivatedRoute) { }
  users: User[];
  ngOnInit() {
    this.route.data.subscribe(data=>{
      this.users= data['users'];  
    })
    //this.loadUsers();
  }

  loadUsers(){
    this.userService.getUsers().subscribe((users:User[]) => {
      this.users= users;
    }, error=> {
      this.alertifyService.error(error);
    });
  }

}
