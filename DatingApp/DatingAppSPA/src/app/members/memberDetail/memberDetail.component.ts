import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { AlertifyService } from 'src/app/services/alertify.service';
import { User } from 'src/app/models/user';
import { ActivatedRoute } from '@angular/router';
import { NgxGalleryImage, NgxGalleryOptions, NgxGalleryAnimation } from '@kolkov/ngx-gallery';
import { TimeagoModule } from 'ngx-timeago';

@Component({
  selector: 'app-memberDetail',
  templateUrl: './memberDetail.component.html',
  styleUrls: ['./memberDetail.component.css']
})
export class MemberDetailComponent implements OnInit {

  constructor(private userService: UserService, private alertifyService: AlertifyService,
    private route: ActivatedRoute) { }
  user: User;
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];
  
  ngOnInit() {
    //this.loadUser();
    this.route.data.subscribe(data => {
      this.user = data['user'];
    });
    this.configureGalleryOptions();
    this.galleryImages = [];
    this.getImages();
  }

  loadUser(){
    this.userService.getUser(this.route.snapshot.params['id']).subscribe((user: User)=>{
      this.user= user;
    }, error=>{
      this.alertifyService.error(error);
    }
    );
  }
  
  configureGalleryOptions(){
    this.galleryOptions = [
      {
        width: '600px',
        height: '400px',
        thumbnailsColumns: 4,
        imageAnimation: NgxGalleryAnimation.Slide
      },
      // max-width 800
      {
        breakpoint: 800,
        width: '100%',
        height: '600px',
        imagePercent: 80,
        thumbnailsPercent: 20,
        thumbnailsMargin: 20,
        thumbnailMargin: 20
      },
      // max-width 400
      {
        breakpoint: 400,
        preview: false
      }
    ];
  }

  getImages(){
    for(const photo of this.user.photos){
      this.galleryImages.push(
        {
          small: photo.url,
          medium: photo.url,
          big: photo.url,
          description: photo.description
        }
      );
    }
  }

}
