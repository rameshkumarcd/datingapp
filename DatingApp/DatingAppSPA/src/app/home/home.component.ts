import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  isInRegisterMode=false;
  constructor() { }

  ngOnInit() {
  }

  registerToggle(){
    this.isInRegisterMode = true;
  }

  val: string='';

  cancelRegisterMode(emitValue:boolean){
    this.isInRegisterMode =emitValue;
  }

}
