import {Routes} from '@angular/router'
import {HomeComponent} from './home/home.component';
import {ListsComponent} from './lists/lists.component';
import {MembersListComponent} from './members/membersList/membersList.component';
import { MemberCardComponent } from './members/memberCard/memberCard.component';
import { MemberDetailComponent } from './members/memberDetail/memberDetail.component';
import {MessagesComponent} from './messages/messages.component';
import {AuthGuard} from './guards/auth.guard';
import {PreventUnsavedChangesGuard} from './guards/preventUnSavedChanges.guard';

import { MemberDetailResolver } from './resolvers/memberDetail';
import { MembersResolver } from './resolvers/members';
import { MemberEditComponent } from './members/memberEdit/memberEdit.component';
import { MemberEditResolver } from './resolvers/memberEditResolver';

export const appRoutes : Routes = [
    {path: '', component: HomeComponent},
    {
        path:'',
        runGuardsAndResolvers: "always",
        canActivate: [AuthGuard],
        children:[
            {path: 'lists', component: ListsComponent},
            {path: 'members', component: MembersListComponent, resolve:{users: MembersResolver}},
            {path: 'members/:id', component: MemberDetailComponent, resolve : {user: MemberDetailResolver}},
            {path: 'member/edit', component: MemberEditComponent, resolve: {user: MemberEditResolver}, canDeactivate:[PreventUnsavedChangesGuard] },
            {path: 'messages', component: MessagesComponent},
            {path: 'memberCard', component: MemberCardComponent},
            
        ]
    },
    
    {path: '**', redirectTo:'', pathMatch: 'full'}
] ;