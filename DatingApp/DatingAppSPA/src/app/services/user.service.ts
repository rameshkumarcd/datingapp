import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
 baseUrl = environment.apiUrl + 'user/';
 
 /*httpOptions = {
   headers : new HttpHeaders({
      'Authorization' : 'Bearer ' + localStorage.getItem('token')
   })
 };*/
 
constructor(private httpClient: HttpClient) { }

public getUser(id: number): Observable<User>{
  return this.httpClient.get<User>(this.baseUrl + id);
}

public getUsers(): Observable<User[]>{
  return this.httpClient.get<User[]>(this.baseUrl);
}

public updateUser(id: number, user: User){
  return this.httpClient.put(this.baseUrl + id, user);
}

public setMainPhoto(userId: number, photoId: number){
  return this.httpClient.post(this.baseUrl + userId + '/photo/' + photoId + '/setMain', {});
}

public deletePhoto(userId: number, photoId: number){
  return this.httpClient.delete(this.baseUrl + userId + '/photo/' + photoId + '/deletephoto', {});
}

}
