import {HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AnonymousSubject } from 'rxjs/internal/Subject';
import {map} from 'rxjs/operators'
import {JwtHelperService} from "@auth0/angular-jwt";
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
token : any;
jwtHelperService : JwtHelperService = new JwtHelperService();

 photoUrl = new BehaviorSubject<string>('test');
 currentPhotoUrl = this.photoUrl.asObservable();
 changeUserPhotoUrl(url:string){
   this.photoUrl.next(url);
 }

constructor(private http: HttpClient) { }
public login(usr: any) {
  //import http client and call api login method
  
  return this.http.post( "http://localhost:5000/api/auth/login",
  usr).pipe(
    map((x: any)=> { 
      let user= x;
      
      if (user){
        
        localStorage.setItem("token", user.token);
        localStorage.setItem("photo", user.photoUrl);
        this.changeUserPhotoUrl(user.photoUrl);
      }
    })
  );
  

}

public register(usr: any){
  return this.http.post("http://localhost:5000/api/auth/register",usr);
}

public IsUserLoggedIn(): boolean{
  let localStorageToken= localStorage.getItem("token");
  return !this.jwtHelperService.isTokenExpired(localStorageToken);
}

}
