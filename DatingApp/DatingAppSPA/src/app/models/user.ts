import { Photo } from "./Photo";

export interface User {
    id:number;
    username:string;
    gender:string;
    age:number;
    knownAs: string;
    interests?: string;
    created: Date;
    lastActive: Date;
    introduction?: string;
    lookingFor?: string;
    city: string;
    country: string;
    mainPhotoUrl: string;
    photos?: Photo[];
}
