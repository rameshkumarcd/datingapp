export interface Photo {
    id: number;
    isMainPhoto: boolean;
    url: string;
    description: string;
}
