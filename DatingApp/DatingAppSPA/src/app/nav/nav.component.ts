import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service';
import {AlertifyService} from '../services/alertify.service';
import {JwtHelperService} from "@auth0/angular-jwt";
import {Router} from "@angular/router";
import { User } from '../models/user';
import { UserService } from '../services/user.service';


@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  model: any={};
  
  username: string='';

  userPhotoUrl : string = null;
  
  constructor(private authService: AuthService,
     private alertifyService: AlertifyService
     , private router: Router, private userService: UserService) { }
    
  ngOnInit() {
    this.model.username="";
    this.model.password="";

    this.authService.currentPhotoUrl.subscribe(p=>this.userPhotoUrl = p);
  }

  login(){
    console.log(this.model);
    this.authService.login(this.model).subscribe(next => {
      
      this.alertifyService.success('logged in');
      //if (this.userPhotoUrl === null){
        
        //console.log(new JwtHelperService().decodeToken(localStorage.getItem("token")));
        /*this.userService.getUser(decodedToken.nameid).subscribe((user: User)=>{
          this.userPhotoUrl= user.mainPhotoUrl;
        }, error=>{
          this.alertifyService.error(error);
        }
        );*/
        
      //}
    }, error=>{
      
      this.alertifyService.error(error);
    },()=>{
      this.router.navigate(['/members']);
    }
    );
    
  }

  isLoggedIn(){
    let loggedIn: boolean=false;
    loggedIn = this.authService.IsUserLoggedIn();
    if (loggedIn){
      let decodedToken= new JwtHelperService().decodeToken(localStorage.getItem("token")); 
      this.username = decodedToken.unique_name;
      this.userPhotoUrl = localStorage.getItem("photo");
    }
    else{
      this.username = '';
    }
    return loggedIn;
  }

  logout(){
    localStorage.removeItem("token");
    this.router.navigate(['/home']);
    this.userPhotoUrl = null;
    this.alertifyService.message('Logged Out.')
  }

}
